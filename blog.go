package main

import (
	"html/template"
	"net/http"
)

const (
	TMPL_ROOT    = "tmpl/"
	CONTENT_ROOT = "content/"
)

// Site config
type BlogConfig struct {
	Title string
	Theme string
}

var blogConfig *BlogConfig = nil

// Articles page struct
type Page struct {
	BlogTitle    string
	ArticleTitle string
	Body         string
}

// Will load page from DB
func loadPage(title string) (*Page, error) {
	body := "Hardcoded blog post test since I ran out of time tonight to code the file storage for articles. Will parse Markdown in the future."
	return &Page{BlogTitle: blogConfig.Title, ArticleTitle: title, Body: body}, nil
}

// Handles viewing of articles
func viewHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/article/"):]
	p, _ := loadPage(title)
	t, _ := template.ParseFiles(TMPL_ROOT + blogConfig.Theme + "/index.html")
	t.Execute(w, p)
}

func main() {
	blogConfig = new(BlogConfig)
	blogConfig.Title = "Antony's Blog"
	blogConfig.Theme = "antony"
	http.HandleFunc("/article/", viewHandler)
	http.HandleFunc("/article/css/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, TMPL_ROOT+blogConfig.Theme+r.URL.Path[len("/article"):])
	})
	http.HandleFunc("/article/js/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, TMPL_ROOT+blogConfig.Theme+r.URL.Path[len("/article"):])
	})
	http.HandleFunc("/article/bower_components/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, TMPL_ROOT+blogConfig.Theme+r.URL.Path[len("/article"):])
	})
	http.ListenAndServe(":80", nil)
}
